# -*- coding: utf-8 -*-
"""
Created on Sun Mar 26 23:09:54 2017

@author: fra
"""

import matplotlib.pyplot as plt
from scoring import Scorer
import os.path

model_dir = '../task_1/model/'

def getXYFromScorer(filename):
    scorer = Scorer()
    scorer.setFilename(filename)
        
    if (os.path.isfile(filename)):
        scorer.loadList(filename)
    else:
        print "Plotter: No scores found"
        exit()
        pass
    
    X = list()
    Y = list()
    
    scores = scorer.scoreList    
    #get sorted results
    for i in range(0, scorer.getMaxDelta()+1):
        for j in scores:
            if (j.Delta == i):
                X.append(j.Delta)
                Y.append(j.F1)            
                continue
    return X,Y
    
X, Y = getXYFromScorer(model_dir + "scores")
X_extra, Y_extra = getXYFromScorer(model_dir + "scores_extra")

plt.plot(X, Y, 'b-o', label='Only Train Set')
plt.plot(X_extra, Y_extra, 'r-h', label='Train and Extra Sets')

plt.legend(loc='lower center', ncol=3, fancybox=True, shadow=True)
plt.show()