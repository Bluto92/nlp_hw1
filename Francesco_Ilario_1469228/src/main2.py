# -*- coding: utf-8 -*-
"""
Created on Wed Mar 22 17:03:12 2017

@author: fra
"""

data_dir = '/home/fra/Projects/NLP/HW_1/nlp_hw1/Francesco_Ilario_1469228/material/task1_data/'

from parsers import TrainParser
trainSet, trainFt = TrainParser(data_dir + 'training.eng.txt').parseAll()
devSet, devFt = TrainParser(data_dir + 'dev.eng.txt').parseAll()
testSet, testFt = TrainParser(data_dir + 'test.eng.txt').parseAll()

labels = ['STR', 'B', 'E', 'M', 'S', 'STOP']
MODEL_NAME = 'model.model'

import scipy

from sklearn.metrics import make_scorer
from sklearn.grid_search import RandomizedSearchCV
from os import path
import sklearn_crfsuite
from sklearn_crfsuite import metrics, CRF
import parsers
import pickle


trainingLimits = [100, 300, 500, 800]

for l in trainingLimits:
    X_train, y_train = TrainParser(data_dir + 'training.eng.txt').parseLimited(l)
    
    delta = 0
    max = 0
    # find the best delta value
    for d in range(0, 0):
        crf = CRF(
            algorithm='ap', 
            all_possible_transitions=True,
            max_iterations=100)
        
        crf.fit(X_train, y_train)
        y_Dpred = crf.predict(devSet)
        
        f1Score = metrics.flat_f1_score(devFt, y_Dpred, labels=labels, average="weighted")
        if (f1Score > max):
            max = f1Score
            delta = d
    
    print "[" + str(l) + "] best-delta: " + str(d) + " f1: " + str(f1Score)

