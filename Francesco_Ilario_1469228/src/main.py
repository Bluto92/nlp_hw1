# -*- coding: utf-8 -*-
"""
Created on Sat Mar 18 11:55:25 2017

@author: fra
"""

#import numpy # for decent arrays
#import sklearn #for crf
#from sklearn import svm

#import pickle # for result

import sklearn_crfsuite
from sklearn_crfsuite import metrics, CRF
import parsers
import pickle

data_dir = '/home/fra/Projects/NLP/HW_1/nlp_hw1/Francesco_Ilario_1469228/material/task1_data/'

trainSet, trainFt = parsers.TrainParser(data_dir + 'training.eng.txt').parseAll()
devSet, devFt = parsers.TrainParser(data_dir + 'dev.eng.txt').parseAll()
testSet, testFt = parsers.TrainParser(data_dir + 'test.eng.txt').parseAll()

labels = ['STR', 'B', 'E', 'M', 'S', 'STOP']
MODEL_NAME = 'model.model'

import scipy

from sklearn.metrics import make_scorer
from sklearn.grid_search import RandomizedSearchCV

from os import path

'''
if (not path.isfile(MODEL_NAME)):
    # define fixed parameters and parameters to search
    crf = CRF(
        algorithm='l2sgd',
        max_iterations=100,
        all_possible_transitions=True
    )
    params_space = {
        'c1': scipy.stats.expon(scale=0.5),
        'c2': scipy.stats.expon(scale=0.05),
    }
    
    # use the same metric for evaluation
    f1_scorer = make_scorer(metrics.flat_f1_score,
                            average='weighted', labels=labels)
    
    # search
    rs = RandomizedSearchCV(crf, params_space,
                            cv=3,
                            verbose=1,
                            n_jobs=-1,
                            n_iter=50,
                            scoring=f1_scorer)
    
    rs.fit(trainSet, trainFt)
    
    print "Best score"
    print rs.best_score_
    print "\nBest Estimator"
    
    print rs.best_estimator_
    
    crf = rs.best_estimator_
    '''    
'''
    c = rs.best_params_
    print c
    crf = CRF(
        algorithm='l2sgd', 
        all_possible_states=True,
        all_possible_transitions=True,
        #averaging=None,
        #c=None,
        c1=c['c1'],
        c2=c['c2'],
        #calibration_candidates=None,
        #calibration_eta=None,
        #calibration_max_trials=None,
        #calibration_rate=None,
        #calibration_samples=None, 
        delta=0,
        #epsilon=None, 
        #error_sensitive=None,
        #gamma=None, 
        #keep_tempfiles=None, 
        #linesearch=None, 
        max_iterations=100,
        #max_linesearch=None, 
        #min_freq=None, 
        #model_filename=MODEL_NAME,
        #num_memories=None, 
        #pa_type=None,
        #period=None,
        #trainer_cls=None,
        #variance=None, 
        #verbose=False
        )
        
    crf.fit(trainSet, trainFt)
   ''' 
'''
else:
    model = open(MODEL_NAME, 'r')
    crf = pickle.load(model)
print "\n actual model"
print crf
'''
delta = 0
max = 0
# find the best delta value
for d in range(0, 0):
    print "\nprocessing delta = " + str(d)
    
    crf = CRF(
        algorithm='l2sgd', 
        all_possible_transitions=True,
        #delta=d,
        max_iterations=100)
    
    crf.fit(trainSet, trainFt)
    y_Dpred = crf.predict(devSet)
    #print metrics.flat_classification_report(devFt, y_Dpred, labels=labels, digits=3)
    f1Score = metrics.flat_f1_score(devFt, y_Dpred, labels=labels, average="weighted")
    print "f1 score: " + str(f1Score)
    if (f1Score > max):
        max = f1Score
        delta = d
    
print "\nbest delta: " + str(delta) + "->"+ str(max)

crf = CRF(
        algorithm='l2sgd', 
        all_possible_states=True,
        all_possible_transitions=True,
        #c1=0,
        #c2=0,
        #delta=delta,
        max_iterations=100)

crf.fit(trainSet, trainFt)
y_Dpred = crf.predict(devSet)
print(metrics.flat_classification_report(devFt, y_Dpred, labels=labels, digits=3))

print "\nUsing best estimator on test data"
y_Tpred = crf.predict(testSet)
print(metrics.flat_classification_report(testFt, y_Tpred, labels=labels, digits=3))

print "\nmodel size: " + str(crf.size_)

print "\nmodel accuracy score on dev: " + str(crf.score(devSet, devFt))
print "\nmodel accuracy score on test: " + str(crf.score(testSet, testFt))

#if (not path.isfile(MODEL_NAME)):
    #pickle.dump(crf, open(MODEL_NAME, "wb"))