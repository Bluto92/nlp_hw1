# -*- coding: utf-8 -*-
"""
Created on Sat Mar 25 15:35:10 2017

@author: fra
"""

import scoring
from sklearn_crfsuite import CRF
from dataFormatter import DataFormatter
from parsers import ExtraTrainParser, WordParser
import threading
import os.path

data_dir = '/home/fra/Projects/NLP/HW_1/nlp_hw1/Francesco_Ilario_1469228/material/task1_data/'
model_dir = '../task_1/model/'
scores_file = model_dir + "scores"
scores_extra_file = model_dir + "scores_extra"

scorer = scoring.Scorer()
scorer.setFilename(scores_file)
scorer.loadList()

scorer_extra = scoring.Scorer()
scorer_extra.setFilename(scores_extra_file)
scorer_extra.loadList()

def crfExtra(delta):
    X_train, y_train = [], []
    X_train_train, y_train_train = DataFormatter().getFormattedData(data_dir + "training.eng.txt", delta)
    
    extra_words, dec_extra_words = ExtraTrainParser(data_dir + "extra-annotations-cleaned.csv").parseAll()    
    X_train_extra, y_train_extra = DataFormatter().getFormattedDataFromSets(extra_words, dec_extra_words, delta)

    #merge trains sets
    for i in range(len(X_train_train)):
        X_train.append(X_train_train[i])
        y_train.append(y_train_train[i])
    for i in range(len(X_train_extra)):
        if (len(X_train_extra[i]) == len(y_train_extra[i])):
            X_train.append(X_train_extra[i])
            y_train.append(y_train_extra[i])    
#        else:
#            print "different number of entry for:\n" + str(X_train_extra[i]) + "\n"+ str(y_train_extra[i])

    X_dev, y_dev = DataFormatter().getFormattedData(data_dir + "dev.eng.txt", delta)
    X_test, y_test = DataFormatter().getFormattedData(data_dir + "test.eng.txt", delta)
    
    crf = CRF(
        algorithm='ap',
        all_possible_transitions=True,
        max_iterations=100)

    crf.fit(X_train, y_train, X_dev, y_dev)
    y_pred = crf.predict(X_test)
    score = scorer.score(delta, y_pred, y_test)
    scorer.saveList()
    print "Actual delta: " + str(score.Delta) + " Score: " + str(score.F1)
    pass

def crf(delta):
    X_train, y_train = DataFormatter().getFormattedData(data_dir + "training.eng.txt", delta)
    X_dev, y_dev = DataFormatter().getFormattedData(data_dir + "dev.eng.txt", delta)
    X_test, y_test = DataFormatter().getFormattedData(data_dir + "test.eng.txt", delta)
    
    crf = CRF(
        algorithm='ap',
        all_possible_transitions=True,
        max_iterations=100)

    crf.fit(X_train, y_train, X_dev, y_dev)
    y_pred = crf.predict(X_test)
    score = scorer.score(delta, y_pred, y_test)
    scorer.saveList()
    print "Actual delta: " + str(score.Delta) + " Score: " + str(score.F1)
    pass

def getMaxDelta():
    deltaMax = 0
    wordMax = ""
    if (os.path.isfile(scores_extra_file)):    
        WordsExtra = WordParser(data_dir + "extra-annotations-cleaned.csv").parseAll()[0]
        for word in WordsExtra:
            if (len(word) > deltaMax):
                deltaMax = len(word)
                wordMax = word
    
    WordsExtra = WordParser(data_dir + "training.eng.txt").parseAll()[0]
    for word in WordsExtra:
        if (len(word) > deltaMax):
            deltaMax = len(word)
            wordMax = word
    
    print "Delta Max: " + str(deltaMax) + ": " + wordMax
    return deltaMax

def getMinDelta(scorer_a):
    return scorer_a.getMaxDelta()

def computeScores(scorer_a):
    threads = list()
    
    maxDelta = getMaxDelta()
    minDelta = getMinDelta(scorer_a)
    print "Minimum Delta: " + str(minDelta)
    for delta in range(minDelta, maxDelta+1):
        if (len(threads) == 4):
            t = threads[0]
            t.join()
            threads.remove(t)
        
        thread = threading.Thread(name = "thread-" + str(delta), target=crf, args = (delta, ))
        thread.start()
        threads.append(thread)
        
        #print "activating "+ thread.getName()
      
    for t in threads:
        #print "waiting for the thread: " + t.getName()
        t.join()
    
    D, f = scorer_a.getHigherResults()
    print "\nBetter delta: " + str(D) + " Score: " + str(f)
    
    scorer_a.saveList()
    pass

computeScores(scorer)

