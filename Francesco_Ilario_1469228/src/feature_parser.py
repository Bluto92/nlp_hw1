# -*- coding: utf-8 -*-
"""
Created on Sat Mar 25 13:36:52 2017

@author: fra
"""

from parsers import WordParser

class FeatureParser(WordParser):
    def __init__(self, data_file):
        super(FeatureParser, self).__init__(data_file)
        self.wordList = list()
        pass

    def parseNext(self):
        word = super(FeatureParser, self).parseNext()
        if word == None:
            return None
            
        init = "<w>"
        trail = "</w>"
        
        wL = list()
        wL.append(init)
        for d in list(word):
            wL.append(d)
        wL.append(trail)
        self.wordList.append(wL)
        return wL
    
    def parseAll(self):
        current = self.parseNext()
        while (current != None):
            current = self.parseNext()
        
        return self.wordList

class DeltaApplyer(object):
    def __init__(self):
        self.featureList = dict()
        pass
    
    def getFeatures(self, wordList, delta, pos):
        self.getFeaturesLeft(wordList, delta, pos, pos-1)
        self.getFeaturesRight(wordList, delta, pos, pos)
        
        return self.featureList
    
    def getFeaturesLeft(self, wordList, delta, pos, current):
        if (current < 0 or pos-current > delta):
            #print "no: current : " + str (current)+ "|\t" + str( pos-current ) + " < " + str(delta) 
            pass
        else:
            w = ""
            #print "passed: range:" + str(range(current, pos))
            for i in range(current, pos):
                w += wordList[i]
            self.featureList["left_"+w] = 1
            
            if (pos-current-1 <= delta):
                self.getFeaturesLeft(wordList, delta, pos, current-1)
        pass
    
    def getFeaturesRight(self, wordList, delta, pos, current):
        if (current >= len(wordList) or current-pos > delta):
            return
        else:
            w = ""
            #print "passed: range:" + str(range(pos, current+1))
            for i in range(pos, current+1):
                w += wordList[i]
            self.featureList["right_" +w] = 1

            if current - pos +1 < delta:
                self.getFeaturesRight(wordList, delta, pos, current+1)                 
        

def getAllFeatures(filename, delta):
    parser = FeatureParser(filename)
    parsedWords = parser.parseAll();
    features = list()
    X = list()
    for word in parsedWords:
        X.append(list(word))
        features.append(getFeaturesForWord(word, delta))
    return X, features

def getFeaturesForWord(word, delta):
    features = list()
    deltaApplyer = DeltaApplyer()
    #print range(len(word))        
    for p in range(len(word)):
        #print "pos: " + str(p) +" "+ word[p]
        p_features = deltaApplyer.getFeatures(word, delta, p)     
        #print p_features
        for f in p_features:
            if (not features.__contains__(f)):
                features.append(f)
        
    return features    

data_dir = '/home/fra/Projects/NLP/HW_1/nlp_hw1/Francesco_Ilario_1469228/material/task1_data/'
X_train, y_train = getAllFeatures(data_dir + "training.eng.txt", 2)

import sklearn_crfsuite
from sklearn_crfsuite import metrics, CRF

crf = CRF(
    algorithm = 'ap',
    max_iterations=100,
    all_possible_transitions=True
)

crf.fit(X_train, y_train)