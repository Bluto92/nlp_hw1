# -*- coding: utf-8 -*-
"""
Created on Fri Mar 24 09:46:08 2017

@author: fra
"""

import string

def generateFeatures(delta):
    mainList = list()
    if (delta > 0):
        auxGenerateFeatures(delta, mainList, "", delta)
    return mainList
    

def auxGenerateFeatures(delta, mainList, status, start):
    '''
        generates all possible features for delta bigO(25^delta)
    '''
    opW = "<w>"
    clW = "</w>"
    
    if (delta > 0):
        alphabet = list(string.ascii_lowercase)
        alphabet.append("<w>")
        alphabet.append("</w>")
        
        for digit in alphabet:
            if (digit == "<w>" and delta != start):
                continue
            if (digit == "</w>" and delta != 1):
                continue
            
            current = status + digit
            mainList.append(current)
            auxGenerateFeatures(delta-1, mainList, current, start)
        
    else:
        return
    

def word2features(word, delta):    
    ''' 
        word: <w>[a-Z]+</w>
        delta: [1-9]*[0-9]+
    '''
    #normalize word
    word = word.lower()
    
    res = list()
    bias = list()
    bias.append("BIAS")
    bias.append(1)

    print bias
    #add bias
    res.append(bias)
        
    #clean word
    cleaning = list(word)
    digits = cleaning[3:len(cleaning)-4]
    
    print digits
    
    for digit in digits:
        featureset = generateFeatures(delta)
        
        
    
    return

word2features("<w>ciao</w>", 1)
features = generateFeatures(1)
print features
print len(features)