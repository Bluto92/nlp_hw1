# -*- coding: utf-8 -*-
"""
Created on Sat Mar 25 20:26:40 2017

@author: fra
"""

def PrecisionScore(y_pred, y_set):
    if (len(y_pred) != len(y_set)):
        print "The sets are not of the same size: predicted: " + str(len(y_pred)) + " golden: " + str(len(y_set))         
        return 0
    
    score = 0
    H = 0
    I = 0
    for j in range(len(y_pred)):
        if (len(y_pred[j]) != len(y_set[j])):
            print "The "+str(j)+ "-th entries of the sets are not of the same length: predicted[" +str(j)+ "]: " + str(len(y_pred[j])) + " golden["+str(j)+"]: " + str(len(y_set[j]))
            return 0
        h, i = evaluatePrecisionOnWords(y_pred[j], y_set[j])
        H += h
        I += i
    
    #print "H: " + str(H) + "\tI: " + str(I)
    
    score = float(H) / float(H+I)
    return score, H, I
    
def evaluatePrecisionOnWords(y_p, y_s):
    H = 0
    I = 0
    for i in range(len(y_s)):
        if (y_s[i] == y_p[i] and y_s[i]== u'E'):
            H += 1
        elif (y_s[i] != y_p[i] and y_p[i] == u'E'):
            I += 1
    return H, I
    
def RecallScore(y_pred, y_set):
    if (len(y_pred) != len(y_set)):
        print "The sets are not of the same size: predicted: " + str(len(y_pred)) + " golden: " + str(len(y_set))         
        return 0
    
    score = 0
    H, D = 0, 0
    for j in range(len(y_pred)):
        if (len(y_pred[j]) != len(y_set[j])):
            print "The "+str(j)+ "-th entries of the sets are not of the same length: predicted[" +str(j)+ "]: " + str(len(y_pred[j])) + " golden["+str(j)+"]: " + str(len(y_set[j]))
            return 0
        h, d = evaluateRecallOnWords(y_pred[j], y_set[j])
        H += h
        D += d
    
    #print "H: " + str(H) + "\tD: " + str(D)
    
    score = float(H) / float(H+D)
    return score, H, D

def evaluateRecallOnWords(y_p, y_s):
    H, D = 0, 0
    for i in range(len(y_s)):
        if (y_s[i] == y_p[i] and y_s[i]== u'E'):
            H += 1
        elif (y_s[i] != y_p[i] and (y_s[i] == u'E' or y_s[i] == u'S')):
            D += 1
    return H, D

def F1Score(y_pred, y_set):
    P, h, i = PrecisionScore(y_pred, y_set)
    R, h, d = RecallScore(y_pred, y_set)
    
#    print "P: " + str(P) + " R: " + str(R)
    F1 = (2 * P * R) / (P + R)
 #   print "F1: " + str(F1)
    return F1

class Score(object):
    def __init__(self, delta, h, i, d, p, r, f1):
        self.Delta = delta 
        self.H = h
        self.I = i
        self.D = d
        self.P = p
        self.R = r
        self.F1 = f1

import pickle
class Scorer(object):
    def __init__(self):
        self.scoreList = list()
        pass

    def setFilename(self, filename):
        self.filename = filename
        pass
    
    def score(self, delta, y_pred, y_gold):
        r, h, i = RecallScore(y_pred, y_gold)
        p, h, d = PrecisionScore(y_pred, y_gold)
        f1 = F1Score(y_pred, y_gold)
        score = Score(delta, h, i, d, p, r, f1)
        self.scoreList.append(score)
        return score
    
    def saveList(self, filename = None):
        if (filename == None):
            filename = self.filename
        scoreFile = open(filename, 'w')
        
        pickle.dump(self.scoreList, scoreFile)
        pass
    
    def loadList(self, filename = None):
        if (filename == None):
            filename = self.filename
                
        scoreFile = open(filename, 'r')
        self.scoreList = pickle.load(scoreFile)
        pass
    
    def getMaxDelta(self):
        maxD = 0        
        for i in self.scoreList:
            if i.Delta > maxD:
                maxD = i.Delta
        
        return maxD
    
    def getHigherResults(self):
        maxD = 0
        maxF1 = 0
        
        for i in self.scoreList:
            if i.F1 > maxF1:
                maxF1 = i.F1
                maxD = i.Delta
        
        return maxD, maxF1
    