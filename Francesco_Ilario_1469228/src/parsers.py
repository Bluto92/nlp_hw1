# -*- coding: utf-8 -*-
"""
Parsers File

Created on Mon Mar 20 11:51:39 2017

@author: fra
"""

class Parser(object):

    def __init__(self, data_file):
        self.data_file = data_file
        self.instance_positions, self.size = self.process()
        return


    def open(self):
        self.FILE = open(self.data_file, 'r')
        return


    def close(self): 
        self.FILE.close()
        self.FILE = None
        return


    def process(self):
        self.open()
        instance_positions = []
        while(1):
            position = self.FILE.tell() 
            line = self.FILE.readline().strip()

            if not line:
                break
            else:
                instance_positions.append(position)
                
        self.close()
        return instance_positions, len(instance_positions)

class ExtraTrainParser(Parser):
    def __init__(self, data_file):
        super(ExtraTrainParser, self).__init__(data_file)
        self.open()
        self.currentPos = 0
        return
    
    def parseNext(self):
        if (self.currentPos >= len(self.instance_positions)):
            self.currentPos = 0
            return None
            
        res = self.parse(self.currentPos)
        self.currentPos += 1
        return res
        
    def parse(self, instance_index):
        self.FILE.seek(self.instance_positions[instance_index])
        line = self.FILE.readline().strip()

        digits = list(line.split('\t')[0])
        word = list()
        word.append("<w>")
        for digit in digits:
            word.append(digit)        
        word.append("</w>") # word
        
#        splittedLine = line.split('\t')
#        lEnd = list()
#        for i in range(1, len(splittedLine)):
#            lEnd.append(splittedLine[i])

        lEnd = line.split('\t')[1:]
        
        lEnd = lEnd[0:len(word)]
        analyzing = list()
        analyzing.append(u"START")
        
        for morph in lEnd:
            if (len(analyzing) == len(word)-1):
                break
            
            if (len(morph) == 1):
                analyzing.append(u"S")
            else:
                analyzing.append(u"B")
                pos = 1
                while (++pos < len(morph)-1):
                    analyzing.append(u"M")
                    pos += 1
                
                analyzing.append(u"E")

        analyzing.append(u"STOP")
        
        return word, analyzing
    
    def parseAll(self):
        words = list()
        features = list()
        for i in range(len(self.instance_positions)):          
            word, feature = self.parse(i)
            words.append(word)
            features.append(feature)
        
        return words, features
    
    def parseLimited(self, limit):
        scanned = 0
        words = list()
        features = list()
        for i in range(len(self.instance_positions)):          
            if (++scanned == limit):
                break
            
            word, feature = self.parse(i)
            words.append(word)
            features.append(feature)
            scanned += 1
        
        return words, features


class TrainParser(Parser):
    
    def __init__(self, data_file):
        super(TrainParser, self).__init__(data_file)
        self.open()
        self.currentPos = 0
        return
    
    def parseNext(self):
        if (self.currentPos >= len(self.instance_positions)):
            self.currentPos = 0
            return None
            
        res = self.parse(self.currentPos)
        self.currentPos += 1
        return res
        
    def parse(self, instance_index):
        self.FILE.seek(self.instance_positions[instance_index])
        line = self.FILE.readline().strip()

        digits = list(line.split('\t')[0])
        word = list()
        word.append("<w>")
        for digit in digits:
            word.append(digit)        
        word.append("</w>") # word
        
        line = line.split('\t')[1] # throw away word, keep analysis
        line = line.split(' ') # split analysis 
        
        lEnd = list()
        for string in line:
            lEnd.append(string.split(':')[0])
        
        lEnd = lEnd[0:len(word)]
        analyzing = list()
        analyzing.append(u"START")
        
        for morph in lEnd:
            if (morph == "~"):
                continue
            
            if (len(analyzing) == len(word)-1):
                break
            
            if (len(morph) == 1):
                analyzing.append(u"S")
            else:
                analyzing.append(u"B")
                pos = 1
                while (++pos < len(morph)-1):
                    analyzing.append(u"M")
                    pos += 1
                
                analyzing.append(u"E")

        analyzing.append(u"STOP")
        
        return word, analyzing
    
    def parseAll(self):
        words = list()
        features = list()
        for i in range(len(self.instance_positions)):          
            word, feature = self.parse(i)
            words.append(word)
            features.append(feature)
        
        return words, features
    
    def parseLimited(self, limit):
        scanned = 0
        words = list()
        features = list()
        for i in range(len(self.instance_positions)):          
            if (++scanned == limit):
                break
            
            word, feature = self.parse(i)
            words.append(word)
            features.append(feature)
            scanned += 1
        
        return words, features

class WordParser(Parser):
    
    def __init__(self, data_file):
        super(WordParser, self).__init__(data_file)
        self.open()
        self.currentPos = 0
        return
    
    def parseNext(self):
        if (self.currentPos >= len(self.instance_positions)):
            self.currentPos = 0
            return None
            
        res = self.parse(self.currentPos)
        self.currentPos += 1
        return res
        
    def parse(self, instance_index):
        self.FILE.seek(self.instance_positions[instance_index])
        line = self.FILE.readline().strip()

        word = line.split('\t')[0]
        features = line.split('\t')[1:]
        
        return word, features
    
    def parseAll(self):
        words = list()
        features = list()
        for i in range(len(self.instance_positions)):
            word, feature = self.parse(i)
            words.append(word)
            features.append(feature)
        
        return words, features
    
    def parseLimited(self, limit):
        scanned = 0
        words = list()
        features = list()
        for i in range(len(self.instance_positions)):          
            if (++scanned == limit):
                break
            
            word, feature = self.parse(i)
            words.append(word)
            features.append(feature)
            scanned += 1
        
        return words, features

'''
tParser = TrainParser('/home/fra/Projects/NLP/HW_1/nlp_hw1/Francesco_Ilario_1469228/material/task1_data/training.eng.txt')
for x in range(len(tParser.instance_positions)):
    word, dec = tParser.parse(x)
    print word + ': ' + str(dec)
'''