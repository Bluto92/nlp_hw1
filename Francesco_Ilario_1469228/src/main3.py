# -*- coding: utf-8 -*-
"""
Created on Sat Mar 25 11:48:20 2017

@author: fra
"""


data_dir = '/home/fra/Projects/NLP/HW_1/nlp_hw1/Francesco_Ilario_1469228/material/task1_data/'

from parsers import TrainParser
trainSet, trainFt = TrainParser(data_dir + 'training.eng.txt').parseAll()
devSet, devFt = TrainParser(data_dir + 'dev.eng.txt').parseAll()
testSet, testFt = TrainParser(data_dir + 'test.eng.txt').parseAll()

labels = ['STR', 'B', 'E', 'M', 'S', 'STOP']
MODEL_NAME = 'model.model'

import scipy

from sklearn.metrics import make_scorer
from sklearn.grid_search import RandomizedSearchCV
from os import path
import sklearn_crfsuite
from sklearn_crfsuite import metrics, CRF
import parsers
import pickle
import featuresDataStructure

fParserTrain = featuresDataStructure.FeatureParser(data_dir + 'training.eng.txt', 1)
X_train, y_train = fParserTrain.parseLimited(100)

print "Train data acquired"
fParserDev = featuresDataStructure.FeatureParser(data_dir + 'dev.eng.txt', 1)
X_dev, y_dev = fParserDev.parseLimited(200)
'''
print "X_dev"
print X_dev

print "y_dev"
print y_dev
'''
print "Dev data acquired"
fParserTest = featuresDataStructure.FeatureParser(data_dir + 'test.eng.txt', 1)
X_test, y_test = fParserTest.parseLimited(100)

print "Test data acquired"


model = CRF(algorithm = 'ap',
            all_possible_transitions=True,
            max_iterations=100)
model.fit(X_train, y_train, X_dev, y_dev)
print "Model fitted"
y_Tpred = model.predict(X_test)
print "Prediction done"
        
f1Score = metrics.flat_f1_score(y_test, y_Tpred, labels=labels, average="weighted")
print "f1 score: " + str(f1Score)
'''
'''
'''
trainingLimits = [100, 300, 500, 800]

for l in trainingLimits:
    X_train, y_train = TrainParser(data_dir + 'training.eng.txt').parseLimited(l)
    
    
    delta = 0
    max = 0
    # find the best delta value
    for d in range(0, 0):
        crf = CRF(
            algorithm='ap', 
            all_possible_transitions=True,
            max_iterations=100)
        
        crf.fit(X_train, y_train)
        y_Dpred = crf.predict(devSet)
        
        f1Score = metrics.flat_f1_score(devFt, y_Dpred, labels=labels, average="weighted")
        if (f1Score > max):
            max = f1Score
            delta = d
    
    print "[" + str(l) + "] best-delta: " + str(d) + " f1: " + str(f1Score)

'''