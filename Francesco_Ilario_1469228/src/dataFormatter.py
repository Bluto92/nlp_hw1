# -*- coding: utf-8 -*-
"""
Created on Sat Mar 25 19:55:05 2017

@author: fra
"""
from parsers import TrainParser

class DataFormatter(object):
    def __init__(self):
        pass
    
    def getFormattedDataFromSets(self, Words, Dec, delta):
        X = []
        for w in Words:
            l = list()
            for i in range(len(w)):        
                l.append(self.digit2features(w, i, delta))
            X.append(l)
        
        y = []
        for s in Dec:
            y.append(s)
        return X, y
        
    
    def getFormattedData(self, filename, delta):
        Parser = TrainParser(filename)
    
        Parsed = Parser.parseAll()
        return self.getFormattedDataFromSets(Parsed[0], Parsed[1], delta)
        
    def digit2features(self, wordList, pos, delta):
        for word in wordList:
            word = word.lower()
        
        features = {
            #'bias': 1.0,
            'digit': wordList[pos],
        }
        delta = delta +1
        
        word = ""
        for d in wordList:
            if (d != "<w>" and d != "</w>"):
                word += d
                
        for d in range(1, delta):
            #left
            if (pos-d >= 1):
                features.update({
                    'left_' + word[pos-d-1:pos-1] : 1
                })
            elif (pos-d == 0):
                features.update({
                    'left_<w>' + word[pos-d:pos-1] : 1
                })
                
            #right
            if (pos+d <= len(word)):
                features.update({
                    'right_' + word[pos:pos+d] : 1
                })
            elif (pos+d == len(word)+1):
                features.update({
                    'right_' + word[pos:pos+d] + "</w>" : 1
                })
            
        return features

'''
dataFormatter = DataFormatter()

wL = list()
wL.append("<w>")
for d in list("abcde"):
    wL.append(d)
wL.append("</w>")
X = []

for i in range(len(wL)):
    X.append(dataFormatter.digit2features(wL, i, 1))
    
print wL
print
print X
'''
