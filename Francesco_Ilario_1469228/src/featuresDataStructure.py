# -*- coding: utf-8 -*-
"""
Created on Fri Mar 24 10:41:34 2017

@author: fra
"""

from parsers import TrainParser
class FeatureParser(TrainParser):
    def __init__(self, data_file, delta):
        super(FeatureParser, self).__init__(data_file)
        self.delta = delta
        return
        
    def parseOne(self):
        X = list()
        y = list()
        elements = self.parseNext()#super(TrainParser, self).parseNext()
        if (elements == None):
            return None, None
            
        words = elements[0]
        labels = elements[1]
        
        for l in range(len(words)):
            word = words[l]
            for i in range(len(word)):
                features = FeaturesDataStructure(word, i, self.delta).features
                X.append(features)
                y.append(list(labels[l][i]))
        
        print X
        print y
        return X,y
        
    def parseAll(self):
        X = list()
        y = list()
        while 1:        
            elements = self.parseOne()
            if (elements == None):
                return X, y
            else:
                X.append(elements[0])
                y.append(elements[1])
    
    def parseLimited(self, num):
        X = list()
        y = list()
        pos = 0;
        while (pos < num):
            pos += 1
            elements = self.parseOne()
            if (elements == None):
                return X, y
            else:
                X.append(elements[0])
                y.append(elements[1])
        return X, y
            
class FeaturesDataStructure(object):
    
    def __init__(self, word, pos, delta):
        self.word = word.lower()                
        self.wordDigit = list()
        self.base = 0
        self.up = len(self.word)
        if (self.word[0:3] == "<w>"):
            self.wordDigit.append("<w>")
            self.base = 4
        lW = len(self.word)
        
        if (self.word[lW-4:lW] == "</w>"):
            self.up = len(self.word)-4
            for i in range(self.base, lW-4):
                self.wordDigit.append(self.word[i])
            self.wordDigit.append("</w>")
        else:
            for i in range(self.base, lW-4):
                self.wordDigit.append(self.word[i])
        
        self.delta = delta
        self.pos = pos        
        self.features = dict()
        #self.features['BIAS'] = 1
        self.populateFeatures()
        return
    
    def populateFeatures(self):
        self.auxPopulateFeaturesLeft(0)
        self.auxPopulateFeaturesRigth(0)
        return
    
    def auxPopulateFeaturesLeft(self, currentDisplacement):
        if (currentDisplacement == self.delta):
            return
        currentDisplacement += 1
        if (self.pos - currentDisplacement <= self.base):
            w = "<w>"
            w += self.word[self.base: self.pos]
            self.features["left_" + w] = 1
            #print "left: " + w            
            return
            
        elif (self.pos > self.up):
            return

        w = self.word[self.pos - currentDisplacement : self.pos]
        #print "left: " + w
        self.features["left_" + w] = 1
        self.auxPopulateFeaturesLeft(currentDisplacement)
        return

    def auxPopulateFeaturesRigth(self, currentDisplacement):
        if (currentDisplacement == self.delta):
            return
        
        if (currentDisplacement + self.pos <= self.base):
            w = "<w>"
            #print "right: " + w
            self.features["right_" + w] = 1
            self.auxPopulateFeaturesRigth(self.base+1)
            return
        elif (currentDisplacement + self.pos == self.up):
            w = self.word[self.pos: self.up]
            w += "</w>"
            #print "right: " + w
            self.features["right_" + w] = 1
            return
        elif (currentDisplacement + self.pos > self.up):
            return

        w = self.word[self.pos: self.pos + currentDisplacement+1]
        #print "right: " + w        
        self.features["right_" + w] = 1
        if (currentDisplacement < self.up):
            self.auxPopulateFeaturesRigth(currentDisplacement+1)
        return

s = "<w>abcdefg</w>"
#for i in range(len(s)):
i=4
fd = FeaturesDataStructure(s, i, 4)
print "[" +str(i) + "] " +str(s[9]) + ": " + str(fd.features)
