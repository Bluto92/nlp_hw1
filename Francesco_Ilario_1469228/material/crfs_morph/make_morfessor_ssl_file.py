
import sys

for line in sys.stdin:

    segmentation = line.strip()
    word = segmentation.replace(' ', '')

    out = word + '\t' + 'MORFESSOR' + '\t'

    begin_segment = True

    for letter in segmentation:        

        if letter == ' ':
            begin_segment = True
            continue

        if begin_segment:
            out += '1 '            
            begin_segment = False
        else:
            out += '0 '            

    print out
    


    

