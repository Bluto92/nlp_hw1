

import time
import numpy
import sys
from optparse import OptionParser
from collections import Counter
import gzip
import cPickle


def train(train_file, token):

    varieties = {'successor' : {}, 'predecessor' : {}}

    TRAIN_FILE = gzip.open(train_file, 'r')

    line = TRAIN_FILE.readline()

    while(line):

        if token:
            count, word = line.strip().split()
        else:
            word = line.strip()

        word = '_' + word + '_'

        for t in range(1, len(word)):

            prefix = word[:t]
            successor = word[t]

            if prefix not in varieties['successor']:

                varieties['successor'][prefix] = Counter()

            varieties['successor'][prefix][successor] += 1
                
        word = word[::-1] 
        
        for t in range(1, len(word)):

            suffix = word[:t]
            predecessor = word[t]

            if suffix not in varieties['predecessor']:

                varieties['predecessor'][suffix] = Counter()

            varieties['predecessor'][suffix][predecessor] += 1                    

        line = TRAIN_FILE.readline()

    # prepare the log-normalized scores (Coltekin, 2010)

    coltekin_counts = {'successor' : Counter(), 'predecessor' : Counter()}
    num_prefixes_of_len = Counter()
    num_suffixes_of_len = Counter()
        
    for prefix, successors in varieties['successor'].items():

        coltekin_counts['successor'][len(prefix)] += len(successors)
        num_prefixes_of_len[len(prefix)] += 1

    for prefix_len, num in coltekin_counts['successor'].items():

        coltekin_counts['successor'][prefix_len] = float(num)/num_prefixes_of_len[prefix_len]

    for suffix, predecessors in varieties['predecessor'].items():

        coltekin_counts['predecessor'][len(suffix)] += len(predecessors)
        num_suffixes_of_len[len(suffix)] += 1

    for suffix_len, num in coltekin_counts['predecessor'].items():

        coltekin_counts['predecessor'][suffix_len] = float(num)/num_suffixes_of_len[suffix_len]

    # put it all together

    model = {'successor' : {}, 'predecessor' : {}}

    for prefix, successors in varieties['successor'].items():

        model['successor'][prefix] = numpy.log(len(successors)/coltekin_counts['successor'][len(prefix)])

    for suffix, predecessors in varieties['predecessor'].items():

        model['predecessor'][suffix] = numpy.log(len(predecessors)/coltekin_counts['predecessor'][len(suffix)])

    return model


def apply(input_file, output_file, model):

    OUTPUT = open(output_file, 'w')

    for word in file(input_file):
        
        word = word.strip()

        line = word + '\tLOG-NORM-SVS\t' 

        word = '_' + word + '_'

        prefix = word[0]
        sv = model['successor'][prefix]        
        line += str(sv) + ' '

        for t in range(2, len(word)-1):

            prefix = word[:t]

            sv = model['successor'].get(prefix, 0)

            line += str(sv) + ' '
            
        OUTPUT.write('%s\n' % line)

#        print "sv:", line

    for word in file(input_file):

        word = word.strip()

        word = '_' + word[::-1] + '_'

        line = ''

        for t in range(2, len(word)):

            suffix = word[:t]

            pv = model['predecessor'].get(suffix, 0)

            line = str(pv) + ' ' + line

        line = word.replace('_', '')[::-1] + '\tLOG-NORM-PVS\t' + line

        OUTPUT.write('%s\n' % line)

    OUTPUT.close()

    return




if __name__ == "__main__":

    tic = time.clock()

    # parse options
    parser = OptionParser("Usage: %prog [options]")
    parser.add_option("--train_file", dest = "train_file", default = None)
    parser.add_option("--model_file", dest = "model_file", default = None)
    parser.add_option("--input_file", dest = "input_file", default = None)
    parser.add_option("--output_file", dest = "output_file", default = None)
    parser.add_option("--token", action = "store_true", dest = "token", default = False)
    parser.add_option("--verbose", action = "store_true", dest = "verbose", default = False)

    (options, args) = parser.parse_args()

    # print options
    
    if options.verbose:
        print
        print "count/apply log-normalized successor and predecessor variety scores (LOG-NORM-SVS and LOG-NORM-PVS)"
        print 
        print "train file:", options.train_file
        print "model file:", options.model_file
        print "input file:", options.input_file
        print "output file:", options.output_file
        print "word tokens:", options.token
        print "verbose:", options.verbose

    # count varieties

    if options.train_file:

        model = train(options.train_file, options.token)

        if options.model_file: # save model

#            pass # save

#            FILE = gzip.GzipFile(options.model_file, 'wb')
#            FILE.write(cPickle.dumps(model, 1))
#            FILE.close()

            FILE = open(options.model_file, 'w')
            cPickle.dump(model, FILE)
            FILE.close()

    else: # load model

        if options.model_file:

#            FILE = gzip.GzipFile(options.model_file, 'rb')
            FILE = open(options.model_file, 'rb') 
            model = cPickle.load(FILE)
            FILE.close()

#            pass # load
        
    # apply to data

    if options.input_file and options.output_file:

        apply(options.input_file, options.output_file, model)
    
    
    if options.verbose:
        print "time consumed:", time.clock() - tic
        print

    
    

    
    



    
    
    
