


import bpr
import copy
import cPickle
import gzip
import numpy
from optparse import OptionParser
import os
import re
import time


class TrainAlgorithm(object):

    # __init__
    def __init__(self):

        return

    def get_id(self):
        return self.id


class Perceptron(TrainAlgorithm):
    # averaged perceptron (Collins, 2002)

    # __init__
    def __init__(self, inference_algorithm, decoder):
        
        super(Perceptron, self).__init__()
                                                             
        self.id = 'perceptron'

        self.inference_algorithm = inference_algorithm
        self.decoder = decoder

        return


    def train(self, parameters, 
              hyperparameters, 
              train_data, 
              devel_data, 
              segmentation_file, 
              reference_file, 
              verbose):
        
        cum_parameters = copy.deepcopy(parameters)
        hyperparameters['number of passes'] = 1
        hyperparameters['number of updates'] = 1

        opt_parameters = None
        opt_hyperparameters = None
        opt_performance = {'target measure' : None}        

        convergence_threshold = 3

        while(1):

            # make a pass over train data

            parameters, cum_parameters, hyperparameters, error_free = self.make_pass(train_data, parameters, cum_parameters, hyperparameters)
            
            parameters = self.compute_average_parameters(parameters, cum_parameters, hyperparameters)

            # decode devel data

            performance = self.decoder.decode(parameters, devel_data, segmentation_file, reference_file)

            if verbose:
                print "\tpass index %d, %s (devel) %.1f" % (hyperparameters['number of passes'], performance['target measure id'], performance['target measure'])

            # check for termination 

            if performance['target measure'] > opt_performance['target measure']:
                
                opt_parameters = copy.copy(parameters)
                opt_hyperparameters = copy.copy(hyperparameters)
                opt_performance = copy.copy(performance)

            elif hyperparameters['number of passes'] - opt_hyperparameters['number of passes'] == convergence_threshold:

                    break

            parameters = self.reverse_parameter_averaging(parameters, cum_parameters, hyperparameters)
            hyperparameters['number of passes'] += 1

        return opt_parameters, opt_hyperparameters, opt_performance


    def make_pass(self, train_data, 
                  parameters, 
                  cum_parameters, 
                  hyperparameters):
        
        error_free = True

        for instance_index in range(train_data.size):            

            # get instance

            observation, len_observation, feature_vector_set, true_state_index_set, true_state_set = train_data.get_instance(instance_index)

            # inference 

            map_state_index_set, map_state_set = self.inference_algorithm.compute_map_state_index_set(parameters, feature_vector_set, len_observation)

            # update parameters

            parameters, cum_parameters, hyperparameters, update_required = self.update_parameters(parameters, cum_parameters, hyperparameters, map_state_index_set, true_state_index_set, feature_vector_set, len_observation)

            hyperparameters['number of updates'] += 1

            if update_required:

                error_free = False

        return parameters, cum_parameters, hyperparameters, error_free


    def update_parameters(self, parameters, 
                          cum_parameters, 
                          hyperparameters,
                          map_state_index_set, 
                          true_state_index_set, 
                          feature_vector_set, 
                          len_observation):
        
        num_updates = hyperparameters['number of updates']

        update_required = False

        for t in range(1, len_observation):

            map_state_index = map_state_index_set[(t-1, t)]
            true_state_index = true_state_index_set[(t-1, t)]
                        
            if map_state_index != true_state_index:
                        
                update_required = True

                feature_vector = feature_vector_set[t]
                ((feature_index_set, activation_set), (transition_feature_index_set, transition_activation_set)) = feature_vector
                
                # update transition parameters
                
                parameters[1][true_state_index, transition_feature_index_set] += transition_activation_set
                parameters[1][map_state_index, transition_feature_index_set] -= transition_activation_set

                cum_parameters[1][true_state_index, transition_feature_index_set] += num_updates*transition_activation_set
                cum_parameters[1][map_state_index, transition_feature_index_set] -= num_updates*transition_activation_set

                # update emission parameters
                
                map_state_index_j = map_state_index_set[t]
                true_state_index_j = true_state_index_set[t]
                        
                if map_state_index_j != true_state_index_j:

                    parameters[0][true_state_index_j, feature_index_set] += activation_set
                    parameters[0][map_state_index_j, feature_index_set] -= activation_set

                    cum_parameters[0][true_state_index_j, feature_index_set] += num_updates*activation_set
                    cum_parameters[0][map_state_index_j, feature_index_set] -= num_updates*activation_set

        return parameters, cum_parameters, hyperparameters, update_required


    def compute_average_parameters(self, parameters, cum_parameters, hyperparameters):

        num_updates = float(hyperparameters['number of updates'])

        parameters[0] -= cum_parameters[0]/num_updates
        parameters[1] -= cum_parameters[1]/num_updates

        return parameters


    def reverse_parameter_averaging(self, parameters, cum_parameters, hyperparameters): 

        num_updates = float(hyperparameters['number of updates'])

        parameters[0] += cum_parameters[0]/num_updates
        parameters[1] += cum_parameters[1]/num_updates

        return parameters


class StateManager(object):

    # __init__
    def __init__(self, train_file):

        self.parser = TrainFileParser(train_file)

        self.state_sets, self.num_states = self.process()

        return


    def process(self):

        # initialize

        self.parser.open()

        state_sets = [['START', 'STOP'], []]

        # fetch states from training data

        for instance_index in range(self.parser.size):

            observation, len_observation, true_state_set = self.parser.parse(instance_index)

            for t in range(1, len_observation):

                state = true_state_set[t]

                if state not in state_sets[0]:
                    state_sets[0].append(state)

        # make state transitions

        for state_i in state_sets[0]:
            for state_j in state_sets[0]:
                state_sets[1].append((state_i, state_j))

        # number of states

        num_states = [len(state_sets[0]), len(state_sets[1])]

        # close parser

        self.parser.close()

        return state_sets, num_states


    def convert_state_set_to_state_index_set(self, state_set, len_observation): 

        if state_set == None:
            
            state_index_set = None

        else:

            state_index_set = {} 

            for t in range(1, len_observation):

                state_i = state_set[t-1]
                state_j = state_set[t]

                try:
                        
                    state_index_set[t] = self.state_sets[0].index((state_j))

                except ValueError:
                    
                    state_index_set[t] = -1                    

                try:
                        
                    state_index_set[(t-1, t)] = self.state_sets[1].index((state_i, state_j))

                except ValueError:
                    
                    state_index_set[(t-1, t)] = -1                    

        return state_index_set


    def convert_state_index_set_to_state_set(self, state_index_set, len_observation): 

        state_set = {} 

        for t in range(1, len_observation):

            try:

                state_index = state_index_set[t]
                
            except:

                state_index = -1

            state_set[t] = self.state_sets[0][state_index]

            try:

                state_index = state_index_set[(t-1, t)]
                
            except:

                state_index = -1

            state_set[(t-1, t)] = self.state_sets[1][state_index]

        return state_set 



class PerformanceMeasure(object):

    def __init__(self, token_eval):

        self.token_eval = token_eval

        return


    def evaluate(self, segmentation_file, goldstd_file):
        
        class BPROptions:
            pass
        bpr_options = BPROptions()
        bpr_options.goldFile = goldstd_file 
        bpr_options.predFile = segmentation_file
        bpr_options.weightFile = None
        bpr_options.micro = False
        bpr_options.strictalts = False
        bpr_options.tokens = False
        if self.token_eval: 
           bpr_options.tokens = True
            
        pre, rec, f, pre_n, rec_n = bpr.run_evaluation(bpr_options)

        performance = {'target measure id' : None, 'target measure' : None, 'all' : []}

        performance['target measure id'] = 'F-measure'
        performance['target measure'] = f*100

        performance['all'].append(('precision', pre*100))
        performance['all'].append(('recall', rec*100))
        performance['all'].append(('F-measure', f*100))

        return performance



class Parser(object):

    def __init__(self, data_file):

        self.data_file = data_file

        self.instance_positions, self.size = self.process()

        return


    def open(self):

        self.FILE = open(self.data_file, 'r')

        return


    def close(self): 

        self.FILE.close()
        self.FILE = None

        return


    def process(self):

        self.open()

        instance_positions = []

        while(1):

            position = self.FILE.tell() 
            line = self.FILE.readline().strip() 

            if not line:
                
                break
            
            else:

                instance_positions.append(position)

        self.close()

        return instance_positions, len(instance_positions)



class TrainFileParser(Parser):

    def __init__(self, data_file):

        super(TrainFileParser, self).__init__(data_file)

        return


    def parse(self, instance_index):

        self.FILE.seek(self.instance_positions[instance_index])
        line = self.FILE.readline().strip()

        observation = ['<s>']
        output = {}

        line = line.split(',')[0] # throw away multiple analyses
        line = line.split('\t')[1] # throw away word, keep analysis
        line = line.split(' ') # split analysis 

        t_word = 1
        for analysis_segment in line:

            if re.match('\:', analysis_segment):

                analysis_segment = analysis_segment.replace('\:', '###')

                if re.match('.+:.+', analysis_segment):
                    segment, tag = analysis_segment.split(':')
                    segment = segment.replace('###', '\:')
                    tag = tag.replace('###', '\:')
                else:
                    segment = analysis_segment
                    segment = segment.replace('###', '\:')
                    
            else:

                if re.match('.+:.+', analysis_segment):
                    segment, tag = analysis_segment.split(':')
                else:
                    segment = analysis_segment

            t_seg = 1
            for char in segment:
                observation.append(char)
                if t_seg == 1:
                    output[t_word] = 'B-SEG'
                else:
                    output[t_word] = 'M-SEG'
                t_seg += 1
                t_word += 1

        output[0] = 'START' 
        output[t_word] = 'STOP' 
        observation.append('</s>')

        # add single-letter morphs

        for t in range(len(observation)-1):
            
            if output[t] == 'B-SEG' and output[t+1] in ['B-SEG', 'STOP']:
                output[t] = 'S-SEG'

        # make doubleton cliques
        for t in range(len(observation)-1):
            output[(t, t+1)] = (output[t], output[t+1])

        return observation, len(observation), output


class DevelFileParser(Parser):

    def __init__(self, data_file, token_eval):

        super(DevelFileParser, self).__init__(data_file)

        self.token_eval = token_eval

        return


    def parse(self, instance_index):

        self.FILE.seek(self.instance_positions[instance_index])
        line = self.FILE.readline().strip()

        if self.token_eval == False:

            observation = ['<s>']

            line = line.split('\t')[0] # throw away analysis, keep word

            t_word = 1
            for char in line:
                observation.append(char)
                t_word += 1

            observation.append('</s>')
 
        else:

            observation = ['<s>']

            line = line.split(' ')[1:] # throw away word frequency
            line = ''.join(line)

            t = 1
            for char in line:
                observation.append(char)
                t += 1

            observation.append('</s>')

        return observation, len(observation)


class TestFileParser(Parser): 

    def __init__(self, data_file):

        super(TestFileParser, self).__init__(data_file)

        return


    def parse(self, instance_index):
        
        self.FILE.seek(self.instance_positions[instance_index])
        line = self.FILE.readline().strip()

        observation = ['<s>']  
        
        t = 1
        for char in line:
            observation.append(char)
            t += 1

        observation.append('</s>')
            

        return observation, len(observation)



class InferenceAlgorithm(object):

    def __init__(self, state_manager):

        self.state_manager = state_manager
        
        return

    def get_id(self):
        return self.id



class Viterbi(InferenceAlgorithm):

    def __init__(self, state_manager):

        super(Viterbi, self).__init__(state_manager)

        self.id = 'Viterbi'

        return

    def compute_map_state_index_set(self, parameters, feature_vector_set, len_observation):
        
        potential_vector_set = self.compute_potential_vector_set(parameters, feature_vector_set, len_observation)

        map_state_index_set = {}

        backtrack_pointers = -1*numpy.ones((len_observation, self.state_manager.num_states[0]))
                                           
        a = None
        
        for t in range(1, len_observation):
                                               
            ab = potential_vector_set[(t-1, t)]
            if a != None:
                a_ab = a.reshape(a.size, 1) + ab # broadcast
            else:
                a_ab = ab
            backtrack_pointers[t, :] = numpy.argmax(a_ab, 0)
            a = numpy.max(a_ab, 0)

        map_state_index_set[t] = int(numpy.argmax(a))

        # backward
                
        for t in range(len_observation-1, 0, -1):
                                          
            map_j = map_state_index_set[t] # t already has likeliest state
            map_i = int(backtrack_pointers[t, map_j])
            map_state_index_set[t-1] = map_i
            map_state_index_set[(t-1, t)] = int(map_i*self.state_manager.num_states[0] + map_j)

        map_state_set = self.state_manager.convert_state_index_set_to_state_set(map_state_index_set, len_observation)

        return map_state_index_set, map_state_set


    def compute_potential_vector_set(self, parameters, feature_vector_set, len_observation):

        # return this
        potential_vector_set = {}

        for t in range(1, len_observation):

            potential_vector = self.compute_potential_vector(parameters, feature_vector_set[t], transition=True)

            potential_vector = potential_vector.reshape((self.state_manager.num_states[0], self.state_manager.num_states[0]))            

            potential_vector_j = self.compute_potential_vector(parameters, feature_vector_set[t], transition=False)

            potential_vector_set[(t-1, t)] = potential_vector+potential_vector_j

        return potential_vector_set


    def compute_potential_vector(self, parameters, feature_vector, transition):

        ((feature_index_set, activation_set), (transition_feature_index_set, transition_activation_set)) = feature_vector

        if transition:

            potential_vector = numpy.sum(parameters[1][:, transition_feature_index_set]*transition_activation_set, 1)

        else:

            potential_vector = numpy.sum(parameters[0][:, feature_index_set]*activation_set, 1)

        return potential_vector




class FeatureManager(object):

    # __init__
    def __init__(self, train_file, ssl_file, max_len_substring):
        
        self.parser = TrainFileParser(train_file)
        self.max_len_substring = max_len_substring

        self.ssl = None
        if ssl_file:
            self.ssl = self.process_ssl_file(ssl_file)

        self.features, self.num_features, self.transition_features, self.num_transition_features = self.process()
        
        
        return


    def process(self):
        
        self.parser.open()

        features = {'BIAS': 0}
        transition_features = {'BIAS': 0}

        for instance_index in range(self.parser.size):

            observation, len_observation, state_set = self.parser.parse(instance_index)

            feature_sets, transition_feature_sets = self.extract_observation_feature_sets(observation, len_observation) 
            
            for t in range(1, len_observation):

                # add feature 
                for (feature, value) in feature_sets[t]:
                    if features.get(feature, -1) == -1:
                        features[feature] = len(features)

            for t in range(1, len_observation):

                # add feature 
                for (transition_feature, transition_value) in transition_feature_sets[t]:
                    if transition_features.get(transition_feature, -1) == -1:
                        transition_features[transition_feature] = len(transition_features)

        self.parser.close()

        return features, len(features), transition_features, len(transition_features)


    def make_feature_vector_set(self, observation, len_observation):

        feature_vector_set = {}

        feature_sets, transition_feature_sets = self.extract_observation_feature_sets(observation, len_observation)

        for t in range(1, len_observation):

            feature_set = feature_sets[t]

            feature_index_set = []
            activation_set = []
            for feature, activation in feature_set:
                feature_index = self.features.get(feature, -1)
                if feature_index > -1:
                    feature_index_set.append(feature_index)
                    activation_set.append(activation) 

            feature_vector = (numpy.array(feature_index_set, dtype=numpy.int), numpy.array(activation_set, dtype=numpy.float))

            transition_feature_set = transition_feature_sets[t]

            transition_feature_index_set = []
            transition_activation_set = []
            for transition_feature, transition_activation in transition_feature_set:
                transition_feature_index = self.transition_features.get(transition_feature, -1)
                if transition_feature_index > -1:
                    transition_feature_index_set.append(transition_feature_index)
                    transition_activation_set.append(transition_activation) 
                
            transition_feature_vector = (numpy.array(transition_feature_index_set, dtype=numpy.int32), numpy.array(transition_activation_set, dtype=numpy.float))

            feature_vector_set[t] = (feature_vector, transition_feature_vector)

        return feature_vector_set


    def extract_observation_feature_sets(self, observation, len_observation):

        if self.ssl:

            word = ''.join(observation[1:len_observation-1])
            
            d = self.ssl.get(word, None)

            if d == None:
                print "WARNING: ssl-file is defined but does not contain word " + word

        else:

            d = None

        # return feature sets in dictionary
        feature_sets = {}       
        feature_sets[0] = [('BIAS', 1), ('substring right <s>', 1)]
        feature_sets[len_observation-1] = [('BIAS', 1), ('substring right </s>', 1)]

        transition_feature_sets = {}
        transition_feature_sets[0] = [('BIAS', 1)] 
        transition_feature_sets[len_observation-1] = [('BIAS', 1)] 

        for t in range(1, len_observation-1):

            # bias
            feature_sets[t] = [('BIAS', 1)] 
            transition_feature_sets[t] = [('BIAS', 1)]

            if d != None:

                for ssl_id, ssl_values in d.items():

                    ssl_feature = 'BIAS' + '+' + ssl_id
                    ssl_value = float(ssl_values[t-1])
                    feature_sets[t].append((ssl_feature, ssl_value))
                    transition_feature_sets[t].append((ssl_feature, ssl_value)) 

            # left substring
            for start in range(max(t-self.max_len_substring, 0), t):

                feature = 'substring left ' + ''.join(observation[start:t])
                feature_sets[t].append((feature, 1))

                if d != None:

                    for ssl_id, ssl_values in d.items():

                        ssl_feature = feature + '+' + ssl_id
                        ssl_value = float(ssl_values[t-1])
                        feature_sets[t].append((ssl_feature, ssl_value))

            # right substring
            for stop in range(t+1, min(t+self.max_len_substring, len_observation)+1):

                feature = 'substring right ' + ''.join(observation[t:stop])
                feature_sets[t].append((feature, 1))

                if d != None:

                    for ssl_id, ssl_values in d.items():

                        ssl_feature = feature + '+' + ssl_id
                        ssl_value = float(ssl_values[t-1])
                        feature_sets[t].append((ssl_feature, ssl_value))

        return feature_sets, transition_feature_sets


    def process_ssl_file(self, ssl_file):

        ssl = {}

        import gzip

        FILE = gzip.open(ssl_file, 'r')

        line = FILE.readline()

        while(line):

            line = line.strip().split('\t')

            if len(line) != 3:
                continue

            word = line.pop(0)

            if word not in ssl: 
                ssl[word] = {}

            ssl_id = line.pop(0)                        
            values = line[0].split() # values is an array of strings

            ssl[word][ssl_id] = values

            line = FILE.readline()

        return ssl


class Decoder(object):

    # __init__
    def __init__(self, inference_algorithm, token_eval):
        
        self.inference_algorithm = inference_algorithm
        self.performance_measure = PerformanceMeasure(token_eval)

        return


    def write(self, observation, len_observation, map_state_set, FILE):

        line = ''.join(observation[1:len_observation-1]) + '\t'

        for t in range(1, len_observation-1):

            if map_state_set[t][0:2] in ['B-', 'S-']:

                if t == 1:                    

                    line += observation[t]
                    
                else:
                        
                    line += ' ' + observation[t] 

            else:

                line += observation[t]
        
        FILE.write('%s\n' % line)

        return


    def decode(self, parameters, data, segmentation_file, reference_file = None):

        FILE = open(segmentation_file, 'w')

        for instance_index in range(data.size):
            
            # get instance
            
            observation, len_observation, feature_vector_set = data.get_instance(instance_index)

            # inference

            map_state_index_set, map_state_set = self.inference_algorithm.compute_map_state_index_set(parameters, feature_vector_set, len_observation)

            # write to file
            
            self.write(observation, len_observation, map_state_set, FILE)

        FILE.close()

        if reference_file != None:

             performance = self.performance_measure.evaluate(segmentation_file, reference_file)

        else:

            performance = None


        return performance


class Data(object):

    # __init__
    def __init__(self, parser, feature_manager, state_manager):

        self.parser = parser
        self.feature_manager = feature_manager
        self.state_manager = state_manager

        self.size = self.parser.size

        return

    def process(self):

        pass

    
    def get_instance(self):

        pass



class TrainData(Data):

    # __init__
    def __init__(self, train_file, feature_manager, state_manager):

        parser = TrainFileParser(train_file)

        super(TrainData, self).__init__(parser, feature_manager, state_manager)

        self.id = 'train data'

        self.feature_vector_sets, self.observations, self.true_state_sets, self.true_state_index_sets, self.instance_index_set = self.process()

        return


    def process(self):

        feature_vector_sets = []
        observations = []
        true_state_sets = []
        true_state_index_sets = []

        self.parser.open()

        for instance_index in range(self.parser.size):

            observation, len_observation, true_state_set = self.parser.parse(instance_index)

            feature_vector_set = self.feature_manager.make_feature_vector_set(observation, len_observation)

            true_state_index_set = self.state_manager.convert_state_set_to_state_index_set(true_state_set, len_observation)

            # add
            feature_vector_sets.append(feature_vector_set)
            observations.append(observation)
            true_state_sets.append(true_state_set)
            true_state_index_sets.append(true_state_index_set)

        self.parser.close()

        instance_index_set = range(self.size)

        return feature_vector_sets, observations, true_state_sets, true_state_index_sets, instance_index_set


    def get_instance(self, instance_index, args = None):
        
        observation = self.observations[instance_index]
        
        len_observation = len(observation)
        
        feature_vector_set = self.feature_vector_sets[instance_index]
        
        true_state_index_set = self.true_state_index_sets[instance_index]
        
        true_state_set = self.true_state_sets[instance_index]

        return observation, len_observation, feature_vector_set, true_state_index_set, true_state_set



class DevelData(Data):

    # __init__
    def __init__(self, devel_file, feature_manager, state_manager, token_eval):

        parser = DevelFileParser(devel_file, token_eval)

        super(DevelData, self).__init__(parser, feature_manager, state_manager)

        self.id = 'devel data'

        self.feature_vector_sets, self.observations = self.process()

        return


    def process(self):

        feature_vector_sets = []
        observations = []

        self.parser.open()

        for instance_index in range(self.parser.size):

            observation, len_observation = self.parser.parse(instance_index)

            feature_vector_set = self.feature_manager.make_feature_vector_set(observation, len_observation)

            # add
            feature_vector_sets.append(feature_vector_set)
            observations.append(observation)

        self.parser.close()

        return feature_vector_sets, observations


    def get_instance(self, instance_index, args = None):
        
        observation = self.observations[instance_index]
        
        len_observation = len(observation)
                
        feature_vector_set = self.feature_vector_sets[instance_index]
                
        return observation, len_observation, feature_vector_set



class TestData(Data):

    # __init__
    def __init__(self, test_file, feature_manager, state_manager):

        parser = TestFileParser(test_file)

        super(TestData, self).__init__(parser, feature_manager, state_manager)

        self.id = 'test data'

        self.feature_vector_sets, self.observations = self.process()
       
        return


    def process(self):

        feature_vector_sets = []
        observations = []

        self.parser.open()

        for instance_index in range(self.parser.size):

            observation, len_observation = self.parser.parse(instance_index)

            feature_vector_set = self.feature_manager.make_feature_vector_set(observation, len_observation)

            # add
            feature_vector_sets.append(feature_vector_set)
            observations.append(observation)

        self.parser.close()

        return feature_vector_sets, observations


    def get_instance(self, instance_index, args = None):
        
        observation = self.observations[instance_index]
                
        len_observation = len(observation)
                
        feature_vector_set = self.feature_vector_sets[instance_index]
                
        return observation, len_observation, feature_vector_set


class TestData(Data):

    # __init__
    def __init__(self, test_file, feature_manager, state_manager):

        parser = TestFileParser(test_file)
        parser.open()

        super(TestData, self).__init__(parser, feature_manager, state_manager)

        self.id = 'test data'

        return

    def remove(self):

        self.parser.close()

        return

    def get_instance(self, instance_index, args = None):
        
        observation, len_observation = self.parser.parse(instance_index)

        len_observation = len(observation)
                
        feature_vector_set =  self.feature_manager.make_feature_vector_set(observation, len_observation)
                
        return observation, len_observation, feature_vector_set



class CRF(object):

    # __init__
    def __init__(self):

        return


    def init(self):

        return


    def save(self, model_file):

        d = {'decoder' : self.decoder, \
                 'feature manager' : self.feature_manager, \
                 'hyperparameters' : self.hyperparameters, \
                 'parameters' : self.parameters, \
                 'state manager' : self.state_manager}

	FILE = gzip.GzipFile(model_file, 'wb')
	FILE.write(cPickle.dumps(d, 1))
	FILE.close()

        return


    def load(self, model_file):

	FILE = gzip.GzipFile(model_file, 'rb')

        d = cPickle.load(FILE)

        self.decoder = d['decoder']
        self.feature_manager = d['feature manager']
        self.hyperparameters = d['hyperparameters']
        self.parameters = d['parameters']
        self.state_manager = d['state manager']

        FILE.close()

        return


    def initialize_parameters(self, feature_manager, state_manager):

        parameters = []
        num_parameters = 0
                
        parameters.append(numpy.zeros((state_manager.num_states[0], feature_manager.num_features), dtype=numpy.float32)) 
        num_parameters += state_manager.num_states[0] * feature_manager.num_features

        parameters.append(numpy.zeros((state_manager.num_states[1], feature_manager.num_transition_features), dtype=numpy.float32)) 
        num_parameters += state_manager.num_states[1] * feature_manager.num_transition_features

        return parameters, num_parameters


    def train(self, train_file, devel_file, segmentation_file, ssl_file, token_eval, verbose):

        # check data set sizes
        train_file_parser = TrainFileParser(train_file)
        devel_file_parser = TrainFileParser(devel_file)
        
        print "\ttrain set size:", train_file_parser.size
        print "\tdevel set size:", devel_file_parser.size
        print
       
        # initialize state manager

        self.state_manager = StateManager(train_file)

        if verbose:
            print "\tlabel set:", self.state_manager.state_sets[0]
            print       

        # initialize decoder
        
        inference_algorithm = Viterbi(self.state_manager)
        self.decoder = Decoder(inference_algorithm, token_eval)

        # initialize hyper parameters

        hyperparameters = {'max len substring (delta)' : 1}

        # initialize performance trackers

        opt_performance = {'target measure' : None}
        convergence_threshold = 3

        while True:

            # initialize feature manager
        
            feature_manager = FeatureManager(train_file, ssl_file, hyperparameters['max len substring (delta)'])

            if verbose:
                print "\tmax len substring (delta):", hyperparameters['max len substring (delta)'] 

            # initialize devel and train data sets
            
            train_data = TrainData(train_file, feature_manager, self.state_manager)
            devel_data = DevelData(devel_file, feature_manager, self.state_manager, token_eval)

            # initialize training algorithm

            inference_algorithm = Viterbi(self.state_manager)

            train_algorithm = Perceptron(inference_algorithm, self.decoder)

            # train
                
            parameters, num_parameters = self.initialize_parameters(feature_manager, self.state_manager)

            if verbose:
                print "\tnumber of features/parameters:", num_parameters

            parameters, hyperparameters, performance = train_algorithm.train(parameters, hyperparameters, train_data, devel_data, segmentation_file, devel_file, verbose)

            if performance['target measure'] > opt_performance['target measure']:

                self.feature_manager = copy.deepcopy(feature_manager)
                self.parameters = copy.deepcopy(parameters)
                self.hyperparameters = copy.deepcopy(hyperparameters)
                opt_performance = copy.deepcopy(performance)
                
            # check if training is continued
            if hyperparameters['max len substring (delta)'] - self.hyperparameters['max len substring (delta)'] == convergence_threshold:
                
                break

            hyperparameters['max len substring (delta)'] += 1

            if verbose:
                print
            
        # print

        if verbose:
            print
            print '\toptimal %s on devel set: %.1f' % (opt_performance['target measure id'], opt_performance['target measure'])        
            print '\toptimal %s: %.0f' % ('max len substring (delta)', self.hyperparameters['max len substring (delta)'])
            print '\toptimal %s: %.0f' % ('passes over train set', self.hyperparameters['number of passes'])
            print 

        # decode devel data with optimized parameters         
        devel_data = DevelData(devel_file, self.feature_manager, self.state_manager, token_eval)
        performance = self.decoder.decode(self.parameters, devel_data, segmentation_file)

        self.feature_manager.ssl = None # a new ssl-file is required at decoding, so no need to keep these

        return


    def apply(self, test_file, segmentation_file, ssl_file, verbose):

        # initialize data
        
        if ssl_file:
            self.feature_manager.ssl = self.feature_manager.process_ssl_file(ssl_file)

        test_data = TestData(test_file, self.feature_manager, self.state_manager)

        if verbose:
            print
            print "\ttest set size:", test_data.size
            print

        # decode

        performance = self.decoder.decode(self.parameters, test_data, segmentation_file)

        test_data.remove()

        return









if __name__ == "__main__":

    tic = time.clock()

    # parse options
    parser = OptionParser("Usage: %prog [options]")
    parser.add_option("--train_file", dest = "train_file", default = None)
    parser.add_option("--devel_file", dest = "devel_file", default = None)
    parser.add_option("--test_file", dest = "test_file", default = None)
    parser.add_option("--segmentation_file", dest = "segmentation_file", default = None)
    parser.add_option("--model_file", dest = "model_file", default = None)
    parser.add_option("--token_eval", action = "store_true", dest = "token_eval", default = False)
    parser.add_option("--ssl_file", dest = "ssl_file", default = None)
    parser.add_option("--verbose", action = "store_true", dest = "verbose", default = False)

    (options, args) = parser.parse_args()

    # print options

    print "options"
    print "\ttrain file:", options.train_file
    print "\tdevel file:", options.devel_file
    print "\ttest file:", options.test_file
    print "\tsegmentation file:", options.segmentation_file
    print "\tmodel file:", options.model_file
    print "\tssl file:", options.ssl_file
    print "\ttoken eval:", options.token_eval
    print "\tverbose:", options.verbose
    print

    if options.train_file: # train model

        print "initialize model"
        m = CRF()
        print "done"
        print
        print "train model"
        print
        m.train(options.train_file, options.devel_file, options.segmentation_file, options.ssl_file, options.token_eval, options.verbose)
        print "done"
        print
        print "save model"
        print "\tmodel file:", options.model_file
        m.save(options.model_file)
        print "done"
        print
        print "time consumed in training:", time.clock() - tic
        print

    else: # load model and apply

        print
        print "initialize model"
        m = CRF()
        print "done"
        print

        print "load model"
        print "\tmodel file:", options.model_file
        m.load(options.model_file)
        print "done"
        print

        print "decode"
        m.apply(options.test_file, options.segmentation_file, options.ssl_file, options.verbose)
        print "done"
        print        
        print "time consumed in decoding:", time.clock() - tic
        print
       




