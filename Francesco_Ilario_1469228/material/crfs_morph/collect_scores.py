#!/usr/bin/python
# -*- coding: iso-8859-15 -*-
#

import sys
import numpy as np

def main(args):
    p_list = []
    r_list = []
    f_list = []
    for line in sys.stdin:
        if line[0:10] == 'precision:':
            p_list.append(float(line[11:]))
        elif line[0:10] == 'recall   :':
            r_list.append(float(line[11:]))
        elif line[0:10] == 'fmeasure :':
            f_list.append(float(line[11:]))
        else:
            pass
    precs = np.array(p_list)
    recalls = np.array(r_list)
    fscores = np.array(f_list)
    
    p_mean = np.mean(precs)
    p_std = np.std(precs)
    r_mean = np.mean(recalls)
    r_std = np.std(recalls)
    f_mean = np.mean(fscores)
    f_std = np.std(fscores)
    
    print
    print "TOTAL. Precision: %.4f; dev. %.4f" % (p_mean, p_std)
    print "TOTAL. Recall:    %.4f; dev. %.4f" % (r_mean, r_std)
    print "TOTAL. F-measure: %.4f; dev. %.4f" % (f_mean, f_std)

if __name__ == "__main__":
    main(sys.argv[1:])
